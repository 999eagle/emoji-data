using System;
using System.Linq;
using NUnit.Framework;

using EmojiData;

namespace Test
{
	[TestFixture]
	public class TestData
	{
		[Test]
		public void TestStuff()
		{
			Assert.IsEmpty(Emoji.All.Where(e => e == null), "null-Emoji were found");
			Assert.AreEqual(Emoji.All.Length, Emoji.EmojiMapping.Count, "Mapping of unicode sequences to emoji doesn't contain all emoji");
		}
	}
}
