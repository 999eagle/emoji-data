using System;
using System.Text.RegularExpressions;
using NUnit.Framework;

using EmojiData;

namespace Test
{
	[TestFixture]
	public class TestParsing
	{
		[Test]
		public void TestShortcodeParsing()
		{
			var shortcode = "anger";
			var singleEmoji = Emoji.KnownEmoji.AngerSymbol;

			Assert.IsTrue(Emoji.TryGetEmojiByShortcode(shortcode, out var parsed), "Parsing emoji by shortcode failed");
			Assert.AreEqual(singleEmoji, parsed, $"parsing emoji by shortcode returned {parsed}, but expected {singleEmoji}");
		}

		[Test]
		public void TestRegex()
		{
			var singleEmoji = "👩";
			var emojiInText = "a👩b";
			var multipleEmoji = "👩👩";
			var joinedEmoji = "👩‍👩‍👧";

			var matches = Emoji.EmojiRegex.Matches(singleEmoji);
			Assert.AreEqual(1, matches.Count, "Found more than a single match");
			var match = matches[0];
			Assert.IsTrue(match.Success, "Match wasn't successful");
			Assert.AreEqual(singleEmoji, match.ToString());

			matches = Emoji.EmojiRegex.Matches(emojiInText);
			Assert.AreEqual(1, matches.Count, "Found more than a single match");
			match = matches[0];
			Assert.IsTrue(match.Success, "Match wasn't successful");
			Assert.AreEqual(singleEmoji, match.ToString());

			matches = Emoji.EmojiRegex.Matches(multipleEmoji);
			Assert.AreEqual(2, matches.Count, "Didn't find exactly 2 matches");
			Assert.IsTrue(matches[0].Success, "Match wasn't successful");
			Assert.AreEqual(singleEmoji, matches[0].ToString());
			Assert.IsTrue(matches[1].Success, "Match wasn't successful");
			Assert.AreEqual(singleEmoji, matches[1].ToString());

			matches = Emoji.EmojiRegex.Matches(joinedEmoji);
			Assert.AreEqual(1, matches.Count, "Didn't match joined emoji");
			match = matches[0];
			Assert.IsTrue(match.Success, "Match wasn't successful");
			Assert.AreNotEqual(singleEmoji, match.ToString());
		}

		[Test]
		public void TestStringParsing()
		{
			var emojiString = "👩";
			var combinedEmojiString = "👩‍🔬";

			Assert.IsTrue(Emoji.TryGetSingleEmoji(emojiString, out var parsed));
			Assert.AreEqual(Emoji.KnownEmoji.Woman, parsed);
			Assert.IsTrue(Emoji.TryGetSingleEmoji(combinedEmojiString, out parsed));
			Assert.AreEqual(Emoji.KnownEmoji.FemaleScientist, parsed);

			var text = "abc";
			var textWithEmoji = "a👩";
			var emojiWithText = "👩a";

			Assert.IsFalse(Emoji.TryGetSingleEmoji(text, out parsed));
			Assert.IsFalse(Emoji.TryGetSingleEmoji(textWithEmoji, out parsed));
			Assert.IsFalse(Emoji.TryGetSingleEmoji(emojiWithText, out parsed));

		}
	}
}
