using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json;

namespace Generator
{
	class Emoji
	{
		public string Name { get; set; }
		[JsonProperty("short_name")]
		public string ShortName { get; set; }
		public string Unified { get; set; }
		public string Category { get; set; }
		[JsonProperty("sort_order")]
		public int SortOrder { get; set; }
		[JsonProperty("skin_variations")]
		public IDictionary<string, EmojiSkinVariation> SkinVariations { get; set; } = new Dictionary<string, EmojiSkinVariation>();

		public IEnumerable<EmojiVariation> GetSkinVariationEmoji() => SkinVariations.Select(v => v.Value.ToEmojiVariation(v.Key, this));

		public virtual string GetCSharpVariableName() =>
			(Name == null ?
				ShortName :
				Name)
				.Split(new[] { ' ', '-', '&', '.', '’', '_' }, StringSplitOptions.RemoveEmptyEntries)
				.Select(s => s.StartsWith("(") && s.EndsWith(")") ? s.Substring(1, s.Length - 2) : s)
				.Select(s => s.Substring(0, 1).ToUpperInvariant() + (s.Length <= 1 ? "" : s.Substring(1).ToLowerInvariant()))
				.Aggregate((a, b) => a + b);

		public string GetRegex() =>
			Unified
				.Split("-")
				.Select(cp => UInt32.Parse(cp, NumberStyles.HexNumber, CultureInfo.CurrentCulture.NumberFormat))
				.Select(cp =>
				{
					if (cp <= 0xFFFF) return $"\\u{cp:X4}";
					else if (cp <= 0x10FFFF)
					{
						var value = cp - 0x010000;
						var high = (UInt16)((value >> 10) | 0xD800);
						var low = (UInt16)((value & 0x03FF) | 0xDC00);
						return $"\\u{high:X4}\\u{low:X4}";
					}
					else throw new Exception();
				})
				.Aggregate((a, b) => a + b)
				+ (!SkinVariations.Any() ? "" :
					GetSkinVariationEmoji()
					.Select(v => $"|{v.GetRegex()}")
					.Aggregate((a, b) => a + b)
				);
	}

	class EmojiVariation : Emoji
	{
		public string VariationKey { get; set; }
		public Emoji Parent { get; set; }

		public override string GetCSharpVariableName() =>
			Parent.GetCSharpVariableName()
			+ $".SkinVariations[{VariationKey}]";
	}

	class EmojiSkinVariation
	{
		public string Unified { get; set; }

		public EmojiVariation ToEmojiVariation(string key, Emoji parent)
		{
			return new EmojiVariation
			{
				Unified = Unified,
				VariationKey = GetVariationKey(key),
				Parent = parent,
			};
		}

		private string GetVariationKey(string key)
		{
			switch (key)
			{
				case "1F3FB":
					return "SkinVariation.Fitzpatrick1_2";
				case "1F3FC":
					return "SkinVariation.Fitzpatrick3";
				case "1F3FD":
					return "SkinVariation.Fitzpatrick4";
				case "1F3FE":
					return "SkinVariation.Fitzpatrick5";
				case "1F3FF":
					return "SkinVariation.Fitzpatrick6";
			}
			throw new Exception();
		}
	}
}
