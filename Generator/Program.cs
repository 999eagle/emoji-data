using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Generator
{
	class Program
	{
		static async Task<int> Main(string[] args)
		{
			var stream = await GetEmojiData();
			if (stream == null) return 1;
			var data = ParseEmojiData(stream);
			if (data == null) return 2;
			using (var fs = File.OpenWrite("EmojiData/Emoji.g.cs"))
			using (var output = new StreamWriter(fs))
			{
				GenerateEmojiCode(data.ToList(), output);
			}
			return 0;
		}

		static async Task<Stream> GetEmojiData()
		{
			Console.Write("Downloading emoji data... ");
			using (var client = new HttpClient())
			{
				var response = await client.GetAsync("https://github.com/iamcal/emoji-data/raw/master/emoji.json");
				if (!response.IsSuccessStatusCode)
				{
					Console.WriteLine($"failed. Status code: {response.StatusCode}");
					return null;
				}
				Console.WriteLine("done.");
				return await response.Content.ReadAsStreamAsync();
			}
		}

		static IEnumerable<Emoji> ParseEmojiData(Stream data)
		{
			try
			{
				Console.Write("Parsing emoji data... ");
				var reader = new JsonTextReader(new StreamReader(data));
				var serializer = JsonSerializer.CreateDefault();
				var emoji = serializer.Deserialize<IEnumerable<Emoji>>(reader);
				Console.WriteLine("done.");
				return emoji;
			}
			catch (Exception ex)
			{
				Console.WriteLine($"failed. Exception: {ex.Message}\n{ex.StackTrace}");
				return null;
			}
		}

		static void GenerateEmojiCode(IList<Emoji> emoji, TextWriter output)
		{
			string fileStart = @"using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

// this file is generated automatically, do not change

namespace EmojiData
{
	public static partial class Emoji
	{
		public static class KnownEmoji
		{";
			string fileEnd = @"	}
}";
			Console.Write("Generating code... ");
			output.WriteLine(fileStart);
			foreach (var e in emoji)
			{
				output.Write($"\t\t\tpublic static readonly SingleEmoji {e.GetCSharpVariableName()} = new SingleEmoji(new UnicodeSequence(\"{e.Unified}\"), \"{e.Name}\", \"{e.ShortName}\", {e.SortOrder}");
				if (e.SkinVariations.Any())
				{
					output.Write(", new Dictionary<SkinVariation, SingleEmoji> { ");
					output.Write(String.Join(", ", e.GetSkinVariationEmoji().Select(v => $"{{ {v.VariationKey}, new SingleEmoji(new UnicodeSequence(\"{v.Unified}\")) }}")));
					output.Write(" }");
				}
				output.WriteLine(");");
			}
			output.WriteLine("\t\t}\n");
			var emojiWithVariations = emoji.SelectMany(e => new[] { e }.Concat(e.GetSkinVariationEmoji()));
			output.WriteLine($"\t\tpublic static readonly SingleEmoji[] All = new[] {{ {String.Join(", ", emojiWithVariations.Select(e => "KnownEmoji." + e.GetCSharpVariableName()))} }};");
			output.WriteLine($"\t\tpublic static readonly Regex EmojiRegex = new Regex(@\"({String.Join("|", emoji.Select(e => e.GetRegex()))})\");");
			output.WriteLine($"\t\tpublic static readonly IReadOnlyDictionary<UnicodeSequence, SingleEmoji> EmojiMapping = new Dictionary<UnicodeSequence, SingleEmoji> {{ {String.Join(", ", emojiWithVariations.Select(e => $"{{ KnownEmoji.{e.GetCSharpVariableName()}.Sequence, KnownEmoji.{e.GetCSharpVariableName()} }}"))} }};");
			output.WriteLine($"\t\tpublic static readonly IReadOnlyDictionary<string, SingleEmoji> ShortcodeMapping = new Dictionary<string, SingleEmoji> {{ {String.Join(", ", emojiWithVariations.Where(e => !String.IsNullOrEmpty(e.ShortName)).Select(e => $"{{ KnownEmoji.{e.GetCSharpVariableName()}.Shortcode, KnownEmoji.{e.GetCSharpVariableName()} }}"))} }};");
			output.WriteLine(fileEnd);
			Console.WriteLine("done.");
		}
	}
}
