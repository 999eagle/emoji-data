using System;

namespace EmojiData
{
	public enum SkinVariation
	{
		Fitzpatrick1_2,
		Fitzpatrick3,
		Fitzpatrick4,
		Fitzpatrick5,
		Fitzpatrick6,
	}
}
