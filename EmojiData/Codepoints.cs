namespace EmojiData
{
	public static class Codepoints
	{
		public static readonly Codepoint ZWJ = new Codepoint("U+200D");
		public static readonly Codepoint VS15 = new Codepoint("U+FE0E");
		public static readonly Codepoint VS16 = new Codepoint("U+FE0F");
	}
}
