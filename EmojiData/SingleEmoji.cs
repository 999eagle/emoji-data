using System;
using System.Collections.Generic;

namespace EmojiData
{
	public class SingleEmoji : IComparable<SingleEmoji>, IEquatable<SingleEmoji>
	{
		public UnicodeSequence Sequence { get; }
		public string Name { get; }
		public string Shortcode { get; }
		public int SortOrder { get; }
		public IReadOnlyDictionary<SkinVariation, SingleEmoji> SkinVariations { get; }

		internal SingleEmoji(UnicodeSequence sequence, string name = "", string shortcode = "", int sortOrder = -1, IReadOnlyDictionary<SkinVariation, SingleEmoji> skinVariations = null)
		{
			Sequence = sequence;
			Name = name;
			Shortcode = shortcode;
			SortOrder = sortOrder;
			SkinVariations = skinVariations ?? new Dictionary<SkinVariation, SingleEmoji>();
		}

		public int CompareTo(SingleEmoji other)
		{
			return SortOrder.CompareTo(other.SortOrder);
		}

		public bool Equals(SingleEmoji other)
		{
			if (other == null) return false;
			return Sequence == other.Sequence;
		}

		public override bool Equals(object obj)
		{
			if (obj is SingleEmoji emoji) return Equals(emoji);
			return false;
		}

		public override int GetHashCode()
		{
			return Sequence.GetHashCode();
		}

		public static bool operator ==(SingleEmoji lhs, SingleEmoji rhs)
		{
			if (Object.ReferenceEquals(lhs, null)) return Object.ReferenceEquals(rhs, null);
			return lhs.Equals(rhs);
		}

		public static bool operator !=(SingleEmoji lhs, SingleEmoji rhs)
		{
			return !(lhs == rhs);
		}

		public override string ToString()
		{
			return Sequence.AsString();
		}
	}
}
