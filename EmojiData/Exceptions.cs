using System;

namespace EmojiData
{
	public class InvalidEncodingException : Exception { }
	public class UnsupportedCodepointException : Exception { }
	public class UnknownEmojiException : Exception { }
}
