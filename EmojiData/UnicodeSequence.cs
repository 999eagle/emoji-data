using System;
using System.Collections.Generic;
using System.Linq;

namespace EmojiData
{
	public class UnicodeSequence : IComparable<UnicodeSequence>, IEquatable<UnicodeSequence>
	{
		private Codepoint[] codepoints;
		public IEnumerable<Codepoint> Codepoints { get => codepoints; }
		public int Length { get => codepoints.Length; }
		public Codepoint this[int index] { get => codepoints[index]; }

		public UnicodeSequence(IEnumerable<Codepoint> codepoints)
		{
			this.codepoints = codepoints.ToArray();
		}

		public UnicodeSequence(string sequence) : this(sequence.Split('-', ',', ' ').Select(c => new Codepoint(c))) { }

		public IEnumerable<byte> AsUTF8()
		{
			foreach (var cp in codepoints)
			{
				foreach (var b in cp.AsUTF8())
				{
					yield return b;
				}
			}
		}

		public IEnumerable<ushort> AsUTF16()
		{
			foreach (var cp in codepoints)
			{
				foreach (var b in cp.AsUTF16())
				{
					yield return b;
				}
			}
		}

		public string AsString()
		{
			return new String(AsUTF16().Select(c => (char)c).ToArray());
		}

		public int CompareTo(UnicodeSequence other)
		{
			for (int i = 0; i < Math.Min(codepoints.Length, other.codepoints.Length); i++)
			{
				if (codepoints[i] != other.codepoints[i]) return codepoints[i].CompareTo(other.codepoints[i]);
			}
			if (codepoints.Length == other.codepoints.Length) return 0;
			if (codepoints.Length < other.codepoints.Length) return -(int)other.codepoints[codepoints.Length].Value;
			return (int)codepoints[other.codepoints.Length].Value;
		}

		public bool Equals(UnicodeSequence other)
		{
			if (other == null) return false;
			return codepoints.Length == other.codepoints.Length && codepoints.Zip(other.codepoints, (a, b) => a == b).All(b => b);
		}

		public override bool Equals(object obj)
		{
			if (obj is UnicodeSequence sequence) return Equals(sequence);
			return false;
		}

		public override int GetHashCode()
		{
			int hash = 0;
			foreach (var cp in codepoints)
			{
				hash = ((hash << 3) | (hash >> 29)) ^ cp.GetHashCode();
			}
			return hash;
		}

		public static bool operator ==(UnicodeSequence lhs, UnicodeSequence rhs)
		{
			if (Object.ReferenceEquals(lhs, null)) return Object.ReferenceEquals(rhs, null);
			return lhs.Equals(rhs);
		}

		public static bool operator !=(UnicodeSequence lhs, UnicodeSequence rhs)
		{
			return !(lhs == rhs);
		}
	}
}
