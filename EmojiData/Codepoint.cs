using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Text;

namespace EmojiData
{
	public class Codepoint : IComparable<Codepoint>, IEquatable<Codepoint>
	{
		public uint Value { get; private set; }

		public Codepoint(uint value)
		{
			Value = value;
		}

		public Codepoint(long value) : this((uint)value) { }

		public Codepoint(string hexValue)
		{
			if (hexValue.StartsWith("0x") || hexValue.StartsWith("U+") || hexValue.StartsWith("u+"))
			{
				hexValue = hexValue.Substring(2);
			}
			if (!UInt32.TryParse(hexValue, NumberStyles.HexNumber, CultureInfo.CurrentCulture.NumberFormat, out var value))
			{
				throw new UnsupportedCodepointException();
			}
			Value = value;
		}

		public IEnumerable<byte> AsUTF8()
		{
			if (Value <= 0x007f)
			{
				yield return (byte)Value;
			}
			else if (Value <= 0x07ff)
			{
				yield return (byte)(0xc0 | (0x1f & (Value >> 6)));
				yield return (byte)(0x80 | (0x3f & Value));
			}
			else if (Value <= 0x0000ffff)
			{
				yield return (byte)(0xe0 | (0x0f & (Value >> 12)));
				yield return (byte)(0x80 | (0x3f & (Value >> 6)));
				yield return (byte)(0x80 | (0x3f & Value));
			}
			else if (Value <= 0x001fffff)
			{
				yield return (byte)(0xf0 | (0x07 & (Value >> 18)));
				yield return (byte)(0x80 | (0x3f & (Value >> 12)));
				yield return (byte)(0x80 | (0x3f & (Value >> 6)));
				yield return (byte)(0x80 | (0x3f & Value));
			}
			else
			{
				throw new UnsupportedCodepointException();
			}
		}

		public IEnumerable<ushort> AsUTF16()
		{
			if (Value <= 0xffff)
			{
				yield return (ushort)Value;
			}
			else if (Value <= 0x0010ffff)
			{
				var val = Value - 0x00010000;
				yield return (ushort)(0xd800 | (0x03ff & (val >> 10)));
				yield return (ushort)(0xdc00 | (0x03ff & val));
			}
			else
			{
				throw new UnsupportedCodepointException();
			}
		}

		public string AsString()
		{
			return new String(AsUTF16().Select(c => (char)c).ToArray());
		}

		public int CompareTo(Codepoint other)
		{
			return Value.CompareTo(other.Value);
		}

		public bool Equals(Codepoint other)
		{
			if (other == null) return false;
			return Value == other.Value;
		}

		public override bool Equals(object obj)
		{
			if (obj is Codepoint cp) return Equals(cp);
			return false;
		}

		public override int GetHashCode()
		{
			return Value.GetHashCode();
		}

		public static bool operator ==(Codepoint lhs, Codepoint rhs)
		{
			if (Object.ReferenceEquals(lhs, null)) return Object.ReferenceEquals(rhs, null);
			return lhs.Equals(rhs);
		}

		public static bool operator !=(Codepoint lhs, Codepoint rhs)
		{
			return !(lhs == rhs);
		}

		public override string ToString()
		{
			return $"U+{Value:X}";
		}
	}
}
