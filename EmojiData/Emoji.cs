using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace EmojiData
{
	public partial class Emoji
	{
		public static bool TryGetSingleEmoji(string s, out SingleEmoji emoji)
		{
			var unicode = s.UnicodeSequence();
			return EmojiMapping.TryGetValue(unicode, out emoji);
		}

		public static SingleEmoji GetSingleEmoji(string s)
		{
			if (TryGetSingleEmoji(s, out var emoji)) return emoji;
			throw new UnknownEmojiException();
		}

		public static bool TryGetEmojiByShortcode(string shortcode, out SingleEmoji emoji)
		{
			return ShortcodeMapping.TryGetValue(shortcode, out emoji);
		}

		public static SingleEmoji GetEmojiByShortcode(string shortcode)
		{
			if (TryGetEmojiByShortcode(shortcode, out var emoji)) return emoji;
			throw new UnknownEmojiException();
		}

		public static void FindAllEmoji(string s, Action<Match, SingleEmoji> callback)
		{
			foreach (Match match in EmojiRegex.Matches(s))
			{
				var emoji = GetSingleEmoji(match.Value);
				callback(match, emoji);
			}
		}
	}
}
