using System;
using System.Collections.Generic;
using System.Text;

namespace EmojiData
{
	public static class StringExtension
	{
		public static IEnumerable<Codepoint> Codepoints(this string s)
		{
			for (int i = 0; i < s.Length; i++)
			{
				if (Char.IsHighSurrogate(s[i]))
				{
					if (s.Length < i + 2 || !char.IsLowSurrogate(s[i + 1]))
					{
						throw new InvalidEncodingException();
					}
					yield return new Codepoint(Char.ConvertToUtf32(s[i], s[++i]));
				}
				else
				{
					yield return new Codepoint((int)s[i]);
				}
			}
		}

		public static UnicodeSequence UnicodeSequence(this string s)
		{
			return new UnicodeSequence(s.Codepoints());
		}
	}
}
