# emoji-data

C# library for easily accessing Emoji

Uses data from https://github.com/iamcal/emoji-data
Unicode encoding based on https://github.com/neosmart/unicode.net
